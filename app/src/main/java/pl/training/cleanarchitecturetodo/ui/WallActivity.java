package pl.training.cleanarchitecturetodo.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import pl.training.cleanarchitecturetodo.R;
import pl.training.cleanarchitecturetodo.api.ApiFactory;
import pl.training.cleanarchitecturetodo.api.WallAPI;
import pl.training.cleanarchitecturetodo.api.model.Message;
import pl.training.cleanarchitecturetodo.api.model.NewMessage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WallActivity extends AppCompatActivity {

    private ListView postsListView;
    private EditText messageContentView;

    private WallAPI wallAPI;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wallAPI = ApiFactory.createApi();

        setContentView(R.layout.activity_wall);
        postsListView = (ListView) findViewById(R.id.posts_list);
        messageContentView = (EditText) findViewById(R.id.message_content);

        View sendButton = findViewById(R.id.send_button);
        if (sendButton != null) {
            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendMessage(messageContentView.getText().toString());
                    messageContentView.setText(null);
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadMessages();
    }

    private void loadMessages() {
        wallAPI.getAllMessages().enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                displayMessages(response.body());
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                displayError(t);
            }
        });
    }

    private void sendMessage(String newMessageContent) {
        wallAPI.sendMessage(new NewMessage(newMessageContent)).enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                loadMessages();
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                displayError(t);
            }
        });
    }

    private void displayMessages(@NonNull List<Message> messages) {
        postsListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, messages));
    }

    private void displayError(@NonNull Throwable t) {
        Toast.makeText(this, "Ups! Error... " + t.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
