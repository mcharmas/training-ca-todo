package pl.training.cleanarchitecturetodo;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class SampleTest {
    @Test
    public void addsInts() {
        //given
        int a = 1;
        int b = 2;

        //when
        int result = a + b;

        //then
        Assertions.assertThat(result).isEqualTo(3);
    }
}
